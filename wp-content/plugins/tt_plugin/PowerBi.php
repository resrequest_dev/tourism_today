<?php

namespace PowerBiEmbed;

class PowerBi
{
    /**
     * Client secret used to authenticate with Azure AD
     * and retrieve the access token.
     *
     * @var string
     */
    protected $clientSecret;

    /**
     * The Azure AD tenant ID.
     *
     * @var string
     */
    protected $tenantId;

    /**
     * The Azure AD app ID.
     *
     * @var string
     */
    protected $appId;

    /**
     * The access token used for authentication with Azure AD services.
     *
     * @var string
     */
    protected $accessToken;

    /**
     * Whether authentication withe Azure AD was successful.
     *
     * @var null|boolean True if authentication was successful, false if authentication failed.
     */
    public $authenticated;

    /**
     * The result of the Azure AD authentication.
     *
     * @var boolean
     */
    private $authenticationResult;

    public function __construct($tenantId, $appId, $clientSecret)
    {
        $this->tenantId = $tenantId;
        $this->appId = $appId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * Authenticate with Azure AD and store the access token.
     *
     * @return bool Whether authentication was successful.
     */
    private function azureADAuthenticate()
    {
        $curl = \curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://login.microsoftonline.com/{$this->tenantId}/oauth2/v2.0/token");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, [
            'client_id' => $this->appId,
            'grant_type' => 'client_credentials',
            'client_secret' => $this->clientSecret,
            'scope' => 'https://analysis.windows.net/powerbi/api/.default'
        ]);

        $result = curl_exec($curl);
        curl_close($curl);

        if (!$result) {
            return false;
        }

        $this->authenticationResult = $result = json_decode($result, true);

        if (isset($result['access_token'])) {
            $this->authenticated = true;
            $this->accessToken = $result['access_token'];

            return true;
        }

        $this->authenticated = false;
        return false;
    }

    /**
     * Gets the Azure AD access token.
     *
     * @return string|false The access token, false if authentication failed.
     */
    function getAzureADAccessToken()
    {
        $authenticated = $this->authenticated;

        if (!$authenticated) {
            $authenticated = $this->azureADAuthenticate();
        }

        if ($authenticated) {
            return $this->accessToken;
        } else {
            return false;
        }
    }

    /**
     * Gets the authentication error if there is one
     *
     * @return array|false The error and error description, false if there is no API error
     */
    function getAuthenticationError()
    {
        $authenticated = $this->authenticated;

        if ($authenticated === null || $authenticated === true) {
            return false;
        }

        return [
            'error' => $this->authenticationResult['error'],
            'errorDescription' => $this->authenticationResult['error_description']
        ];
    }

    /**
     * Get report data
     *
     * @param string $token
     * @return false|array Report data. False if an error occurred.
     */
    function getReport(string $groupId, string $reportId)
    {
        $token = $this->getAzureADAccessToken();

        if (!$token) {
            return false;
        }

        $reportsUrl = "https://api.powerbi.com/v1.0/myorg/groups/{$groupId}/reports/{$reportId}";

        $curl = \curl_init();
        curl_setopt($curl, CURLOPT_URL, $reportsUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer {$token}",
        ));

        $result = curl_exec($curl);
        curl_close($curl);

        if (empty($result)) {
            return false;
        }

        $report = json_decode($result, true);

        return $report;
    }

    /**
     * Get the embed token for the reports
     *
     * @param string $token
     * @param array $reports
     * @return string Embed token. False if an error occurred
     */
    function getEmbedToken(array $report)
    {
        $token = $this->getAzureADAccessToken();

        if (!$token) {
            return false;
        }

        $datasets = [];
        $reportRequest = [];
        $reportRequest[] = [
            'id' => $report['id'],
            'allowEdit' => false
        ];

        if (!in_array($report['datasetId'], array_column($datasets, 'id'))) {
            $datasets[] = [
                'id' => $report['datasetId']
            ];
        }

        $request = json_encode([
            'datasets' => $datasets,
            'reports' => $reportRequest
        ]);

        $curl = \curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.powerbi.com/v1.0/myorg/GenerateToken');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            "Authorization: Bearer {$token}",
        ));

        $result = curl_exec($curl);
        curl_close($curl);

        if (empty($result)) {
            return false;
        }

        $embedToken = json_decode($result, true);

        return $embedToken;
    }
}