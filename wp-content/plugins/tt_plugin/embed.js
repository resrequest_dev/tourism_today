const MINUTES_BEFORE_EXPIRATION = 45;
const INTERVAL_TIME = 30000;
let report = null;
let tokenExpiration;

let groupId = jQuery('#tourismTodayPbiGroupId').val();
let reportId = jQuery('#tourismTodayPbiReportId').val();

jQuery('#powerBiReport').hide();
setTimeout(function () {

    let token = getCachedToken(groupId, reportId);

    if (token) {
        createReport(token.accessToken, token.embedUrl, token.reportId);
    } else {
        getReport().then(function (data) {
            createReport(data.accessToken, data.embedUrl, data.reportId);
        });
    }
});

function getReport() {
    let nonce = jQuery('#tourismTodayPbiNonce').val();

    return new Promise(function (resolve, reject) {
        jQuery.ajax({
            method: 'POST',
            url: '/wp-json/tourism-today-pbi/v1/get-report',
            data: {
                _wpnonce: nonce,
                groupId: groupId,
                reportId: reportId
            },
            success: function (data, status, xhr) {
                data.groupId = groupId;
                setToken(data);
                resolve(data);
            },
            error: function (data) {
                console.log('Failed to fetch report');
                powerBiContainer
                jQuery('#tourismTodayPbiLoader').hide();
                jQuery('#powerBiReport').show();
                reject(data);
            }
        });
    });
}

function setToken(data) {
    let tokens = sessionStorage.getItem('tourismTodayPowerBi');
    if (tokens) {
        tokens = JSON.parse(tokens);
    } else {
        tokens = [];
    }

    let tokenIndex = tokens.findIndex(function(token) {
        if (data.groupId == token.groupId && data.reportId == token.reportId) {
            return true;
        } else {
            return false;
        }
    });

    if (tokenIndex > -1) {
        tokens[tokenIndex] = data;
    } else {
        tokens.push(data);
    }

    token = data;
    tokenExpiration = data.token.expiration;

    sessionStorage.setItem('tourismTodayPowerBi', JSON.stringify(tokens));
}

function getCachedToken(groupId, reportId) {
    let tokens = sessionStorage.getItem('tourismTodayPowerBi');
    if (tokens) {
        tokens = JSON.parse(tokens);
    } else {
        tokens = [];
    }

    let tokenIndex = tokens.findIndex(function(token) {
        if (groupId == token.groupId && reportId == token.reportId) {
            return true;
        } else {
            return false;
        }
    });

    if (tokenIndex > -1) {
        token = tokens[tokenIndex];
        tokenExpiration = token.token.expiration;

        if (tokenValid()) {
            return tokens[tokenIndex];
        }
    }

    return false;
}

function createReport(accessToken, embedUrl, reportId) {
    // Get models. models contains enums that can be used.
    var models = window['powerbi-client'].models;

    // We give All permissions to demonstrate switching between View and Edit mode and saving report.
    var permissions = models.Permissions.All;

    var config = {
        type: 'report',
        tokenType: models.TokenType.Embed,
        accessToken: accessToken,
        embedUrl: embedUrl,
        id: reportId,
        permissions: permissions,
        settings: {
            panes: {
                filters: {
                    visible: false
                },
                pageNavigation: {
                    visible: false
                }
            }
        }
    };

    // Get a reference to the embedded report HTML element
    var embedContainer = document.getElementById('powerBiReport');

    // Embed the report and display it within the div container.
    report = powerbi.embed(embedContainer, config);

    // Report.off removes a given event handler if it exists.
    report.off("loaded");

    // Report.on will add an event handler which prints to Log window.
    report.on("loaded", function () {
    });

    // Report.off removes a given event handler if it exists.
    report.off("rendered");

    // Report.on will add an event handler which prints to Log window.
    report.on("rendered", function () {
    });

    report.on("error", function (event) {
        report.off("error");
    });

    report.off("saved");
    report.on("saved", function (event) {
        if (event.detail.saveAs) {
        }
    });

    setInterval(() => checkTokenAndUpdate(reportId, groupId), INTERVAL_TIME);

    // Add a listener to make sure token is updated after tab was inactive
    document.addEventListener("visibilitychange", function () {
        // Check the access token when the tab is visible
        if (!document.hidden) {
            checkTokenAndUpdate(reportId, groupId)
        }
    });

    jQuery('#tourismTodayPbiLoader').hide();
    jQuery('#powerBiReport').show();
}

function tokenValid() {
    // Get the current time
    const currentTime = Date.now();
    const expiration = Date.parse(tokenExpiration);

    // Time until token expiration in milliseconds
    const timeUntilExpiration = expiration - currentTime;
    const timeToUpdate = MINUTES_BEFORE_EXPIRATION * 60 * 1000;

    // Update the token if it is about to expired
    if (timeUntilExpiration <= timeToUpdate) {
        return false;
    }

    return true;
}

function checkTokenAndUpdate(reportId, groupId) {
    if (!tokenValid()) {
        updateToken(reportId, groupId);
    }
}

function updateToken(reportId, groupId) {
    if (report) {
        // Generate a new embed token or refresh the user Azure AD access token
        getReport(reportId, groupId).then(function (data) {
            // Update the new token expiration time
            tokenExpiration = data.token.expiration;

            report.setAccessToken(data.embedToken);
        });
    }
}