<?php

/**
 * Plugin Name: ResRequest Power BI Embedded
 * Plugin URI: https://www.resrequest.com/
 * Description: Allows embedding of Power BI reports using a shortcode
 * Version: 1.1
 * Author: Keenan Smith
 * Author URI: http://resrequest.com/
 **/

use PowerBiEmbed\PowerBi;

include(__DIR__ . '/PowerBi.php');

function powerBiShortcode($attributes)
{
    $html = '';

    // Fetch report and embed token
    if (empty($attributes['group_id'] || empty($attributes['id']))) {
        return 'Invalid group ID or report ID';
    }

    $reportId = $attributes['id'];
    $groupId = $attributes['group_id'];

    $tenantId = get_option('pb_tenant_id');
    $appId = get_option('pb_app_id');
    $clientSecret = get_option('pb_client_secret');

    // Generate embed HTML
    $values = [
        'nonce' => wp_create_nonce('wp_rest'),
        'reportId' => $reportId,
        'groupId' => $groupId
    ];

    $embedHtml = file_get_contents(__DIR__ . '/embed.html');
    $embedHtml = preg_replace_callback('{{[\S]+}}', function ($matches) use ($values) {
        foreach ($matches as $match) {
            $field = substr($match, 2, -2);

            if (isset($values[$field])) {
                return $values[$field];
            } else {
                return '';
            }
        }
    }, $embedHtml);

    $html = $embedHtml;

    $embedJavascript = file_get_contents(__DIR__ . '/embed.js');
    $html .= '<script>' . $embedJavascript . '</script>';

    return $html;
}
add_shortcode('power-bi', 'powerBiShortcode');

function testAuthorisation()
{
    $tenantId = get_option('pb_tenant_id');
    $appId = get_option('pb_app_id');
    $clientSecret = get_option('pb_client_secret');

    if (empty($tenantId) || empty($appId) || empty($clientSecret)) {
        return;
    }

    $pBi = new PowerBi($tenantId, $appId, $clientSecret);

    $token = $pBi->getAzureADAccessToken();

    if ($token) {
        $class = 'notice notice-success';
        $message = __('Azure AD authentication successful!');
    } else {
        $error = $pBi->getAuthenticationError();
        $class = 'notice notice-error';
        $message = __("Azure AD authentication failed!<br>Error: {$error['error']}<br>Description: {$error['errorDescription']}");
    }

    printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), $message);
}


add_action('admin_menu', 'addMenu');
function addMenu()
{
    add_options_page('Power BI Options', 'Power BI', 'manage_options', 'power_bi_options', 'addOptions');
}

function addOptions()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

?>
    <div class="wrap">
        <h2>Power BI settings</h2>
        <form method="post" action="options.php">
            <?php
            testAuthorisation();
            settings_fields('power_bi_options');
            do_settings_sections('power_bi_options');
            submit_button();
            ?>
        </form>
    </div>
<?php
}


add_action('admin_init', 'setupSections');
function setupSections()
{
    add_settings_section('power_bi_credentials', 'Azure AD service principal credentials', false, 'power_bi_options');
}

add_action('admin_init', 'setupFields');
function setupFields()
{
    $fields = [
        [
            'uid' => 'pb_tenant_id',
            'label' => 'Tenant ID',
            'section' => 'power_bi_credentials',
            'type' => 'text',
            'options' => false,
            'placeholder' => '',
            'helper' => '',
            'supplemental' => '',
            'default' => ''
        ],
        [
            'uid' => 'pb_app_id',
            'label' => 'App ID',
            'section' => 'power_bi_credentials',
            'type' => 'text',
            'options' => false,
            'placeholder' => '',
            'helper' => '',
            'supplemental' => '',
            'default' => ''
        ],
        [
            'uid' => 'pb_client_secret',
            'label' => 'Client secret',
            'section' => 'power_bi_credentials',
            'type' => 'password',
            'options' => false,
            'placeholder' => '',
            'helper' => '',
            'supplemental' => '',
            'default' => ''
        ],
    ];

    foreach ($fields as $field) {
        add_settings_field($field['uid'], $field['label'], 'fieldsCallback', 'power_bi_options', $field['section'], $field);
        register_setting('power_bi_options', $field['uid']);
    }
}


function fieldsCallback($arguments)
{
    $value = get_option($arguments['uid']); // Get the current value, if there is one
    if (!$value) { // If no value exists
        $value = $arguments['default']; // Set to our default
    }

    switch ($arguments['type']) {
        case 'text':
            printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value);
            break;
        case 'password':
            printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value);
            break;
        case 'textarea':
            printf('<textarea name="%1$s" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>', $arguments['uid'], $arguments['placeholder'], $value);
            break;
        case 'select':
            if (!empty($arguments['options']) && is_array($arguments['options'])) {
                $options_markup = ’;
                foreach ($arguments['options'] as $key => $label) {
                    $options_markup .= sprintf('<option value="%s" %s>%s</option>', $key, selected($value, $key, false), $label);
                }
                printf('<select name="%1$s" id="%1$s">%2$s</select>', $arguments['uid'], $options_markup);
            }
            break;
    }

    if ($helper = $arguments['helper']) {
        printf('<span class="helper"> %s</span>', $helper);
    }

    if ($supplimental = $arguments['supplemental']) {
        printf('<p class="description">%s</p>', $supplimental);
    }
}


/**
 * API
 */

// ----------------------------------------------------------------------------

add_action('rest_api_init', function () {
    register_rest_route('tourism-today-pbi/v1', '/get-report', [
        'methods' => 'POST',
        'callback' => 'getReport',
        'permission_callback' => '__return_true'
    ]);
});

function getReport($request)
{
    $body = $request->get_params();
    if (empty($body['groupId'] || empty($body['reportId']))) {
        return ['error' => 'Incorrect group ID or report ID'];
    }

    $reportId = $body['reportId'];
    $groupId = $body['groupId'];

    $tenantId = get_option('pb_tenant_id');
    $appId = get_option('pb_app_id');
    $clientSecret = get_option('pb_client_secret');

    $pBi = new PowerBi($tenantId, $appId, $clientSecret);
    $report = $pBi->getReport($groupId, $reportId);

    if ($report) {
        $embedToken = $pBi->getEmbedToken($report);
    }

    $result = [
        'nonce' => wp_create_nonce('wp_rest'),
        'accessToken' => $embedToken['token'],
        'embedUrl' => $report['embedUrl'],
        'reportId' => $report['id'],
        'token' => $embedToken
    ];

    return $result;
}

function enqueueStyle() {
    wp_enqueue_style( 'tourism-today-pbi-style', plugins_url('embed.css', __FILE__), [], '1.0.0', 'all' );
}
add_action( 'wp_enqueue_scripts', 'enqueueStyle' );