<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '3d482e7638db0511ea2f8ee697fb075af698a78e',
        'name' => 'palasthotel/cron-logger',
        'dev' => true,
    ),
    'versions' => array(
        'palasthotel/cron-logger' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '3d482e7638db0511ea2f8ee697fb075af698a78e',
            'dev_requirement' => false,
        ),
    ),
);
