<?php

/**
 * Plugin Name: ResRequest Tourism.Today
 * Plugin URI: https://www.resrequest.com/
 * Description: Handles operations for the Tourism.Today website
 * Version: 1.1
 * Author: Keenan Smith
 * Author URI: http://resrequest.com/
 **/


/**
 * VAT percentage used to calcuate tax
 */
const TT_VAT = 0.15;

/**
 * The exchange rate to fallback to if it cannot be fetched
 */
const FALLBACK_EXCHANGE_RATE_USD_ZAR = 14.5;
 
/**
 * The interval to check the login session.
 */
const SESSION_VALIDATE_INTERVAL = 30000;

/**
 * TT tiers to Wordpress role map
 */
const TIER_ROLE_MAP = [
    'starter' => 'pmpro_role_8',
    'free' => 'um_tourism-today-free',
    'lite' => 'pmpro_role_4',
    'pro' => 'pmpro_role_1',
    'creator' => 'pmpro_role_3'
];

/** 
 * Landing redirect page ID.
 * Redirects user to landing page based on user role.
 */
const LANDING_REDIRECT_PAGE = 2276;

/**
 * Map user role to landing page ID
 */
const ROLE_LANDING_PAGE_MAP = [
    TIER_ROLE_MAP['starter'] => 3401,
    TIER_ROLE_MAP['free'] => 1050,
    TIER_ROLE_MAP['lite'] => 2280,
    TIER_ROLE_MAP['pro'] => 1042,
    TIER_ROLE_MAP['creator'] => 2121,
];


function onRedirect()
{
    handleLandingRedirect();
}
add_action('template_redirect', 'onRedirect');


/**
 * Returns the user's TT tier.
 *
 * @return string user tier
 */
function getUserTier($userId)
{
    $userObject = new WP_User($userId);
    if (empty($userObject)) {
        return false;
    }

    foreach (array_reverse(TIER_ROLE_MAP) as $tier => $role) {
        if (in_array($role, $userObject->roles)) {
            return $tier;
        }
    }

    return false;
}

/**
 * Redirects the user to a landing page based on their user role
 *
 * @return void
 */
function handleLandingRedirect()
{
    $redirectUrl = false;
    $pageId = get_queried_object_id();
    if (
        !current_user_can('administrator') &&
        (is_page(LANDING_REDIRECT_PAGE) || in_array($pageId, ROLE_LANDING_PAGE_MAP))
    ) {
        if (is_user_logged_in()) {
            $userId = get_current_user_id();
            $tier = getUserTier($userId);

            if ($tier !== false) {
                $redirectUrl = get_permalink(ROLE_LANDING_PAGE_MAP[TIER_ROLE_MAP[$tier]]);
            } else {
                $redirectUrl = home_url();
            }
        } else {
            $redirectUrl = home_url();
        }
    }

    if ($redirectUrl !== false) {
        $currentUrl = get_permalink($pageId);
        if ($currentUrl != $redirectUrl) {
            wp_redirect($redirectUrl, 301);
            exit;
        }
    }
}

function enqueueScripts()
{
    if (is_user_logged_in()) {
        wp_enqueue_script('session_validator', plugins_url('js/session_validator.js', __FILE__), [], '1.0');
        wp_localize_script('session_validator', 'sessionValidator', [
            'nonce' => wp_create_nonce('wp_rest'),
            'redirectUrl' => home_url(),
            'interval' => SESSION_VALIDATE_INTERVAL
        ]);
    }
}
add_action('wp_enqueue_scripts', 'enqueueScripts');

function bouncerBeforeBounce()
{

    if (isRestApiRequest()) {
        return false;
    } else {
        return true;
    }
}
// add_action('wp_bouncer_login_flag', 'bouncerBeforeBounce');


function pmproLevelCostText($r, $level, $tags, $short)
{
    $output = false;

    if (!$short) {
        if ($level->initial_payment == '0.00') {
            $price = 'Free';
        } else {
            $price = pmpro_formatPrice($level->initial_payment);
        }

        if ($level->trial_limit != '1' && (pmpro_formatPrice($level->initial_payment) == pmpro_formatPrice($level->trial_amount))) {
            $output = sprintf(__('The price for membership is <strong>%1$s for the first %2$d %3$s</strong>, then <strong>%4$s per %5$s</strong>', 'paid-memberships-pro'), $price, $level->trial_limit + 1, pmpro_translate_billing_period($level->cycle_period, $level->trial_limit), pmpro_formatPrice($level->billing_amount), pmpro_translate_billing_period($level->cycle_period));
        }
    }

    if ($output !== false) {
        return $output;
    } else {
        return $r;
    }
}
add_filter('pmpro_level_cost_text', 'pmproLevelCostText', 15, 4);


function pmproTax($tax, $values, $order)
{
    if ($values['billing_country'] == 'ZA') {
        $tax = round((float) $values['price'] * TT_VAT, 2);
    }

    return $tax;
}
add_filter("pmpro_tax", "pmproTax", 10, 3);


function pmProInvoicePdfCustomVariables($replacements, $user, $morder)
{
    $replacements['{{company_details}}'] = '';

    $companyName = get_user_meta($user->ID, 'company_name', true);
    $companyTaxNumber = get_user_meta($user->ID, 'company_tax_number', true);

    if (!empty($companyName) || !empty($companyTaxNumber)) {
        $companyDetails = '<p>';

        if (!empty($companyName)) {
            $companyDetails .= '<strong>Company Name:</strong> ' . $companyName;
        }

        if (!empty($companyName) && !empty($companyTaxNumber)) {
            $companyDetails .= '<br>';
        }

        if (!empty($companyTaxNumber)) {
            $companyDetails .= '<strong>Company Tax Number:</strong> ' . $companyTaxNumber;
        }
        $companyDetails .= '</p>';
        $replacements['{{company_details}}'] = $companyDetails;
    }

    $exchangeRate = getExchangeRate();
    $replacements['{{exchange_rate}}'] = $exchangeRate;
    $replacements['{{za_customer_msg}}'] = '';
    if (
        (!empty($morder->billing->country) && $morder->billing->country == 'ZA') ||
        (!empty($morder->billing_country ) && $morder->billing_country  == 'ZA')
    ) {
        $total = pmpro_round_price($morder->total * $exchangeRate);
        $subTotal = pmpro_round_price($morder->subtotal * $exchangeRate);
        $tax = pmpro_round_price($morder->tax * $exchangeRate);
        $replacements['{{za_customer_msg}}'] = 'Customers located in South Africa are charged an additional 15% VAT. For this invoice:<br>' .
            "Subscription: R{$subTotal}<br>" .
            "Tax (15%): R{$tax}<br>" .
            "Total: R{$total}";
    }

    return $replacements;
}
add_filter('pmpro_pdf_invoice_custom_variables', 'pmProInvoicePdfCustomVariables', 10, 3);


function pmProRequiredBillingFields($fields)
{
    global $bfirstname, $bcountry, $bemail;

    $fields = [];
    $fields["bfirstname"] = $bfirstname;
    $fields["bemail"] = $bemail;
    $fields["bcountry"] = $bcountry;

    return $fields;
}
add_action("pmpro_required_billing_fields", "pmProRequiredBillingFields", 40);

function pmProMemberProfileEditUserObjectFields($fields)
{
    unset($fields['display_name']);

    return $fields;
}
add_action("pmpro_member_profile_edit_user_object_fields", "pmProMemberProfileEditUserObjectFields", 40);


/**
 * Add company name and tax number to checkout fields
 *
 * @return void
 */
function pmProCheckoutFields()
{
    // Don't break if Register Helper is not loaded
    if (!function_exists('pmprorh_add_registration_field')) {
        return false;
    }

    $fields = [];
    $fields[] = new PMProRH_Field(
        'company_name',
        'text',
        [
            'label' => 'Company',
            'profile' => false,
            'required' => false,
        ]
    );

    $fields[] = new PMProRH_Field(
        'company_tax_number',
        'text',
        [
            'label' => 'Company tax number',
            'profile' => false,
            'required' => false,
        ]
    );

    foreach ($fields as $field) {
        pmprorh_add_registration_field(
            'checkout_boxes',
            $field
        );
    }
}
add_action('init', 'pmProCheckoutFields');

function pmProAfterUpgrade($userId, $morder)
{
    $discountCode = $morder->getDiscountCode();
    if (!empty($discountCode) && $discountCode->code == 'contributor') {
        update_user_meta($userId, 'contributor', true);
    }
}
add_action("pmpro_after_checkout", "pmProAfterUpgrade", 10, 2);


function rr_pmpro_checkout_level( $level ) {	
	// Bail if no level.
	if ( empty( $level ) ) {
		return $level;
	}

    $userId = get_current_user_id();

    $contributor = get_user_meta($userId, 'contributor', false);

    if ($contributor) {
        $level->initial_payment = $level->initial_payment - 30;
        $level->billing_amount = $level->billing_amount - 30;
    }

    if ( $level->initial_payment < 0 ) {
        $level->initial_payment = 0;
    }
    if ( $level->billing_amount < 0 ) {
        $level->billing_amount = 0;
    }
   
    
    return $level;
}

add_filter( "pmpro_checkout_level", "rr_pmpro_checkout_level", 15, 1 );
/**
 * Add download link to PDF
 *
 * @return void
 */
function pmProInvoiceDownload($pmProInvoice)
{
    $invoiceId = $pmProInvoice->code;
    echo '<li><a target="_blank" href="/wp-admin/?pmpropdf=' . $invoiceId . '" style="color:#c9252f;font-weight:bold;">Download PDF</a></li>';
}
add_action('pmpro_invoice_bullets_bottom', 'pmProInvoiceDownload');


/**
 * Move PM Pro required field asterisks to label
 *
 * @return void
 */
function pmProAsteriskFix()
{
?>
    <script>
        jQuery(document).ready(function() {
            // Remove old asterisks
            jQuery('span.pmpro_asterisk').remove();

            jQuery('.pmpro_required').each(function(index, field) {
                var label = jQuery(field).siblings('label');
                label.html(label.html() + ' <span style="color:#c9252f;">*<span>');
            });

            jQuery('.pmpro_change_password-field').each(function(index, field) {
                var label = jQuery(field).children('label');
                label.html(label.html() + ' <span style="color:#c9252f;">*<span>');
            });
        });
    </script>
<?php
}
add_action('wp_footer', 'pmProAsteriskFix');


/**
 * Modify data sent to payfast before checkout.
 * Converts currency before checkout from USD to ZAR
 *
 * @param array $data Payfast data
 * @return $data
 */
function pmproPayfastData($data) {
    $exchangeRate = getExchangeRate();
    // Convert from USD to ZAR

    $data['amount'] = pmpro_round_price($data['amount'] * $exchangeRate);
    $data['recurring_amount'] = pmpro_round_price($data['recurring_amount'] * $exchangeRate);

    return $data;
}
add_filter( 'pmpro_payfast_data', 'pmproPayfastData' );

/**
 * Gets the exchange rate for the current day and caches it
 *
 * @return float
 */
function getExchangeRate() {
    // Make sure a page with the slug 'exchange-rate-usd-zar' exists
    if ($post = get_page_by_path('exchange-rate-usd-zar', OBJECT)) {
        $postId = $post->ID;
    } else {
        return FALLBACK_EXCHANGE_RATE_USD_ZAR;
    }

    $metaKey = 'tt_exchange_rate_usd_zar_' . (new \DateTime())->format('Y_m_d');

    if (empty($exchangeRate = get_post_meta($postId, $metaKey, true))) {
        $curl = curl_init('https://exchange-rates.abstractapi.com/v1/live/?api_key=d357bafeb22b4a709fe91a4a3c2e5b98&base=USD&target=ZAR');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($response, true);

        if (isset($result['exchange_rates']['ZAR'])) {
            $exchangeRate = $result['exchange_rates']['ZAR'];
            add_post_meta($postId, $metaKey, $exchangeRate, true);
        } else {
            // Use fallback exchange rate
            return FALLBACK_EXCHANGE_RATE_USD_ZAR;
        }
    }

    return (float) $exchangeRate;
}

// function custom_pmproeewe_email_frequency( $settings = array() ) {
// 	$settings = array(
// 		10 => '',
// 		30 => '',
// 		60 => '',
// 	);
// 	return $settings;
// }
// add_filter( 'pmproeewe_email_frequency_and_templates', 'custom_pmproeewe_email_frequency', 10, 1);


/**
 * API
 */

// ----------------------------------------------------------------------------


add_action('rest_api_init', function () {
    // Validate login session
    register_rest_route('tourism-today/v1', '/logged-in', [
        'methods' => 'GET',
        'callback' => 'apiIsLoggedIn',
        'permission_callback' => '__return_true'
    ]);
});

function apiIsLoggedIn()
{
    return [
        'nonce' => wp_create_nonce('wp_rest'),
        'userLoggedIn' => is_user_logged_in()
    ];
}


function cronUpdatePfSubscriptions()
{
    global $wpdb;

    require_once(PMPRO_DIR . '/classes/class.memberorder.php');

    $logPrefix = 'tt_update_pf_subscriptions | ';

    // Get active memberships
    $memberships = $wpdb->get_results(
        'SELECT DISTINCT
            wp_pmpro_memberships_users.membership_id,
            wp_pmpro_memberships_users.user_id
        FROM
            wp_pmpro_memberships_users
        WHERE
            wp_pmpro_memberships_users.status = "active"',
        ARRAY_A
    );

    // Subscriptions to be updated
    $subscriptions = [];
    foreach ($memberships as $membership) {
        $order = new MemberOrder();
        if ($order->getLastMemberOrder($membership['user_id'])) {
            if (empty($order->paypal_token)) {
                // Attempt to locate paypal_token
                if ($originalOrder = $order->get_original_subscription_order() && !empty($originalOrder->paypal_token)) {
                    $order = $originalOrder;
                } else {
                    $subscriptionOrder = $wpdb->get_results(
                        'SELECT
                            code
                        FROM
                            wp_pmpro_membership_orders
                        WHERE
                            session_id = "' .  esc_sql($order->session_id) . '"
                            AND paypal_token !=  "" LIMIT 1'
                    );

                    if (empty($subscriptionOrder) || !$order->getMemberOrderByCode($subscriptionOrder[0]->code)) {
                        continue;
                    }
                }
            }

            $membershipLevel = $order->getMembershipLevel();
            if (empty($membershipLevel)) {
                continue;
            }

            $subscriptionToken = $order->paypal_token;
            $amount = $membershipLevel->billing_amount;

            $trial = false;
            if ($order) {
                $discount = $order->getDiscountCode();

                if (
                    $discount &&
                    $membershipLevel->cycle_number <= $membershipLevel->trial_limit
                ) {
                    $amount = $membershipLevel->trial_amount;
                    $trial = true;
                }
            }

            $tax = $order->getTaxForPrice($amount);

            $taxed = false;
            if (!empty($tax)) {
                $amount = $amount + $tax;
                $taxed = true;
            }

            $subscriptions[] = [
                'amount' => $amount,
                'token' => $subscriptionToken,
                'user' => $membership['user_id'],
                'membershipLevel' => $membershipLevel,
                'trial' => $trial,
                'taxed' => $taxed,
                'tax' => $tax
            ];
        }
    }

    $exchangeRate = getExchangeRate();

    error_log($logPrefix . 'Exchange rate: ' . $exchangeRate);
    error_log($logPrefix . 'Subscriptions to update: ' . count($subscriptions));

    foreach ($subscriptions as $subscription) {
        $newAmount = pmpro_round_price($subscription['amount'] * $exchangeRate);
        $user = get_user_by('id', $subscription['user']);

        error_log($logPrefix . '========');
        error_log($logPrefix . 'Level: ' . $subscription['membershipLevel']->name);
        error_log($logPrefix . 'Level amount: $' . $subscription['amount']);
        error_log($logPrefix . 'Converted amount: R' . $newAmount);

        if ($subscription['taxed']) {
            error_log($logPrefix . 'Taxed: yes');
        } else {
            error_log($logPrefix . 'Taxed: no');
        }

        error_log($logPrefix . 'Tax amount: R' . pmpro_round_price($subscription['tax'] * $exchangeRate));

        if ($subscription['trial']) {
            error_log($logPrefix . 'Trial: yes');
        } else {
            error_log($logPrefix . 'Trial: no');
        }

        error_log($logPrefix . 'Subscription token: ' . $subscription['token']);
        error_log($logPrefix . 'User ID: ' . $subscription['user']);
        error_log($logPrefix . 'User name: ' . $user->user_firstname . ' ' . $user->user_lastname);
        error_log($logPrefix . 'User email: ' . $user->user_email);
        error_log($logPrefix . '--------');

        $response = updateSubscription($subscription['token'], $newAmount * 100);

        if (isset($response['status']) && $response['status'] == 'success') {
            error_log($logPrefix . 'Status: success');
        } else {
            error_log($logPrefix . 'Status: failure');
        }

        error_log($logPrefix . 'Response: ' . print_r($response, true));
        error_log($logPrefix . '========');
        error_log($logPrefix . '');
    }
}
add_action( 'cronUpdatePfSubscriptions', 'cronUpdatePfSubscriptions', 10, 0 );

function updateSubscription($token, $amount)
{
    $curl = curl_init();

    $postFields = [
        'amount' => $amount
    ];

    $headerFields = [
        'merchant-id' => pmpro_getOption('payfast_merchant_id'),
        'version' => 'v1',
        'timestamp' => (new \DateTime)->modify('+2 hours')->format('Y-m-d\Th:i:s'),
    ];

    $signature = generateApiSignature(array_merge($headerFields, $postFields), 'Minimumof12characters');

    $headerFields['signature'] = $signature;

    $output = [];
    foreach ($headerFields as $key => $field) {
        $output[] = $key . ': ' . $field;
    }

    $testing = '';
    $environment = pmpro_getOption('gateway_environment');
    if ($environment === 'sandbox' || $environment === 'beta-sandbox') {
        $testing = '?testing=true';
    }

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.payfast.co.za/subscriptions/' . $token . '/update' . $testing,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PATCH',
        CURLOPT_POSTFIELDS => 'amount='.$amount,
        CURLOPT_HTTPHEADER => $output,
        CURLINFO_HEADER_OUT => true
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    if (!empty($response)) {
        return json_decode($response, true);
    }

    return false;
}


/**
 * Generate signature for API
 * @param array $pfData (all of the header and body values to be sent to the API)
 * @param null $passPhrase
 * @return string
 */
function generateApiSignature($pfData, $passPhrase = null) {

    // Construct variables
    foreach ($pfData as $key => $val) {
        $data[$key] = stripslashes($val);
    }

    if ($passPhrase !== null) {
        $pfData['passphrase'] = $passPhrase;
    }

    // Sort the array by key, alphabetically
    ksort($pfData);

    // Normalise the array into a parameter string
    $pfParamString = '';
    foreach ($pfData as $key => $val) {
        if ($key !== 'signature') {
            $pfParamString .= $key . '=' . urlencode($val) . '&';
        }
    }

    // Remove the last '&amp;' from the parameter string
    $pfParamString = substr($pfParamString, 0, -1);

    return md5($pfParamString);
}


function tourismTodayPluginActivation()
{
    if (!wp_next_scheduled('cronUpdatePfSubscriptions')) {
        wp_schedule_event(time(), 'daily', 'cronUpdatePfSubscriptions');
    }

}
add_action( 'init', 'tourismTodayPluginActivation');