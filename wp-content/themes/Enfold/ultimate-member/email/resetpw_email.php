<div style="background: #f2f2f2; padding: 10px 0;"></div>
<div style="max-width: 560px; padding: 20px; background: #ffffff; border-radius: 5px; margin: 40px auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #666;">
<div style="color: #444444; font-weight: normal;">
<div style="text-align: center; font-weight: 600; font-size: 26px; padding: 10px 0; border-bottom: solid 3px #eeeeee;"><img class="aligncenter" style="align: center;" src="https://alpha.tourism.today/wp-content/uploads/2020/09/Tourism-Today_Logo.png" alt="centered image" width="50%" height="50%" /></div>
<div style="clear: both;"> </div>
</div>
<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;">We received a request to reset the password for your account. <span style="font-size: 14px; display: block;">If you made this request, click the link below to change your password:</span></div>
<div style="padding: 10px 0 50px 0; text-align: center;"><a style="background: #014256; color: #fff; padding: 12px 30px; text-decoration: none; border-radius: 3px; letter-spacing: 0.3px;" href="{password_reset_link}">Reset your password</a></div>
<div style="padding: 15px; background: #eee; border-radius: 3px; text-align: center;">If you didn't make this request, ignore this email or <a style="color: #014256; text-decoration: none;" href="mailto:support@tourism.today">report it to us</a>.</div>
</div>
<div style="color: #999; padding: 20px 30px;">
<div>Thank you!</div>
<div>The <a style="color: #014256; text-decoration: none;" href="{site_url}">{site_name}</a> team</div>
</div>
</div>