<div style="background: #f2f2f2; padding: 10px 0;"> </div>
<div style="max-width: 560px; padding: 20px; padding-top: 20px; background: #ffffff; border-radius: 5px; margin: 40px auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #666;">
<div style="color: #444444; font-weight: normal;">
<div><img class="aligncenter" style="align: center;" src="https://alpha.tourism.today/wp-content/uploads/2020/09/Tourism-Today_Logo.png" alt="centered image" width="50%" height="50%" /></div>
<hr /></div>
<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;">Thank you for signing up!<span style="display: block;">Your account is now active.</span></div>
<div style="padding: 10px 0 50px 0; text-align: center;"><a style="background: #014256; color: #fff; padding: 12px 30px; text-decoration: none; border-radius: 3px; letter-spacing: 0.3px;" href="{login_url}">Login to our site</a></div>
<div style="padding: 20px;">If you have any problems, please Contact us at <a style="color: #014256; text-decoration: none;" href="mailto:support@tourism.today">support@tourism.today</a></div>
</div>
<div style="color: #999; padding: 20px 30px;">
<div>Thank you!</div>
<div>The <a style="color: #014256; text-decoration: none;" href="{site_url}">{site_name}</a> team</div>
</div>
</div>