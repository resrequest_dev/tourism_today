<div style="background: #f2f2f2; padding: 10px 0;"></div>
<div style="max-width: 560px; padding: 20px; background: #ffffff; border-radius: 5px; margin: 40px auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #666;">
<div style="color: #444444; font-weight: normal;">
<div style="text-align: center; font-weight: 600; font-size: 26px; padding: 10px 0; border-bottom: solid 3px #eeeeee;"><img class="aligncenter" style="align: center;" src="https://alpha.tourism.today/wp-content/uploads/2020/09/Tourism-Today_Logo.png" alt="centered image" width="50%" height="50%" /></div>
<div style="clear: both;"> </div>
</div>
<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;">Your account is now deactivated.</div>
<div style="padding: 15px; background: #eee; border-radius: 3px; text-align: center;">If you want your account to be re-activated, please <a style="color: #014256; text-decoration: none;" href="mailto:support@tourism.today">contact us</a>.</div>
</div>
<div style="color: #999; padding: 20px 30px;">
<div>Thank you!</div>
<div>The <a style="color: #014256; text-decoration: none;" href="{site_url}">{site_name}</a> team</div>
</div>
</div>